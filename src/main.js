// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

const slugify = require("@sindresorhus/slugify");

import DefaultLayout from "~/layouts/Default.vue";

import "~/assets/scss/main.scss";

export default function (Vue, { router, head, isClient }) {
  Vue.component("Layout", DefaultLayout);

  router.afterEach((to, from) => {
    // do not rewrite build paths
    if (process.isServer) {
      return;
    }

    let destPath = to.fullPath.split("/"),
      codename = destPath.pop();
    codename = codename ? codename : destPath.pop();

    if (
      codename != slugify(codename, { decamelize: false }) &&
      destPath.pop() == "device"
    ) {
      let targetPath =
        "/device/" + slugify(codename, { decamelize: false }) + "/";

      router.push(
        {
          path: targetPath
        },
        function () {
          console.log("Redirected from " + to.fullPath + " to " + targetPath);
        },
        function () {
          console.log(
            "Failed to redirect (current location is " + targetPath + ")"
          );
        }
      );
    }
  });
}
