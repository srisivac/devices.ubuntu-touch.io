let Validator = require("validatorjs");

class ObjectValidator {
  constructor(data, rules) {
    // Add custom validators and strings for Validator.js
    this.failedProperties = {};
    this.data = data;
    this.rules = rules;

    let containsInArray = function (value, requirement, attribute) {
      let exists;
      try {
        exists = value.some((el) => el[requirement[0]] == requirement[1]);
      } catch (e) {
        exists = false;
      }
      if (!exists) {
        if (!this.failedProperties[attribute])
          this.failedProperties[attribute] = [];
        this.failedProperties[attribute].push(
          requirement[1] + " " + requirement[0] + " is missing for " + attribute
        );
      }
      return exists;
    }.bind(this);

    let containsInArrayIf = function (value, requirement, attribute) {
      if (value[requirement[3]] == requirement[4]) {
        return containsInArray(
          value[requirement[2]],
          [requirement[0], requirement[1]],
          attribute
        );
      } else {
        return true;
      }
    };

    Validator.register("contains", containsInArray.bind(this), ":attribute");
    Validator.register(
      "contains_if",
      containsInArrayIf.bind(this),
      ":attribute"
    );

    this.validation = new Validator(data, rules, {
      required: ":attribute is missing",
      alpha: ":attribute must contain only alphabetic characters",
      alpha_dash:
        ":attribute may only contain alpha-numeric characters, as well as dashes and underscores",
      alpha_num: ":attribute must be alphanumeric",
      between: {
        numeric: ":attribute must be between :min and :max",
        string: ":attribute must be between :min and :max"
      },
      in: ":attribute value is invalid",
      present: ":attribute must be present (but can be empty)",
      required_if: ":attribute is required when :other is :value",
      string: ":attribute must be a string",
      url: ":attribute format is invalid"
    });
  }

  // Validate the data
  passes() {
    return this.validation.passes();
  }

  // Return validation errors
  errors() {
    function getFromPath(obj, path) {
      try {
        for (
          var i = 0, path = path.split("."), len = path.length;
          i < len;
          i++
        ) {
          obj = obj[path[i]];
        }
        return obj;
      } catch (e) {
        return null;
      }
    }

    let notValid = this.validation.errors.all();

    // Set the correct issue description
    for (const key in notValid) {
      let toReplace = notValid[key].indexOf(key);
      if (toReplace != -1) {
        notValid[key].splice(toReplace, 1);
        notValid[key].push(...this.failedProperties[key]);
      }
    }

    // Replace portStatus.* with categoryName
    for (const key in notValid) {
      let replaceWith = getFromPath(this.data, key + ".categoryName");
      if (replaceWith)
        for (const i in notValid[key]) {
          notValid[key][i] = notValid[key][i].replace(
            /portStatus\.[0-9]*/g,
            replaceWith + " in portStatus"
          );
        }
    }

    // Put all the errors in an array
    let notValidArray = [];
    for (const key in notValid) {
      notValidArray.push(...notValid[key]);
    }

    return notValidArray;
  }
}

module.exports = ObjectValidator;
