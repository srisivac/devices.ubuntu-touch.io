---
name: "Planet Computers Cosmo Communicator"
deviceType: "phone"
buyLink: "https://store.planetcom.co.uk/products/cosmo-communicator"
portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "x"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "+"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+-"
        bugTracker: "https://github.com/ubports/ubuntu-touch/issues/1657" # not working on some providers ootb
      - id: "dualSim"
        value: "x"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "+"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "-"
        bugTracker: "https://github.com/ubports/ubuntu-touch/issues/1629" # carrier loss after ~24 hours
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "+"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "+"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "rtcTime"
        value: "+"
      - id: "sdCard"
        value: "+"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "x"
      - id: "wirelessExternalMonitor"
        value: "-"
        bugTracker: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues/92" # aethercast not working
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+-"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "+"
      - id: "nfc"
        value: "-"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "+-"
        bugTracker: "https://github.com/ubports/repowerd/pull/14" # ALS sensor reports data, just not used by MW
      - id: "fingerprint"
        value: "+"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "+"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "+"
      - id: "adb"
        value: "+"
      - id: "wiredExternalMonitor"
        value: "+-"
deviceInfo:
  - id: "cpu"
    value: "Octa-core ARM Cortex-A53, ARM Cortex-A73 (4x 2.0 GHz + 4x 2.1 GHz cores)"
  - id: "chipset"
    value: "MediaTek Helio P70, MT6771T"
  - id: "gpu"
    value: "ARM Mali-G72 MP3 @ 900 MHz, 3 cores"
  - id: "rom"
    value: "128 GB, eMMC"
  - id: "ram"
    value: "6 GB, DDR3"
  - id: "android"
    value: "9.0 (Pie)"
  - id: "battery"
    value: "4200 mAh, Li-Polymer"
  - id: "display"
    value: '5.99" IPS, 1080 x 2160 (403 PPI), Rounded corners'
  - id: "rearCamera"
    value: "24MP, LED flash"
  - id: "frontCamera"
    value: "5MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "171 mm x 79.3 mm x 17.3 mm"
  - id: "weight"
    value: "326 g"
contributors:
  - name: TheKit
    forum: https://forums.ubports.com/user/thekit
    photo: ""
  - name: Davide Guidi
    forum: https://github.com/dguidipc
    photo: ""
externalLinks:
  - name: "Telegram"
    link: "https://t.me/ubports"
    icon: "telegram"
  - name: "Report a bug"
    link: "https://gitlab.com/ubports/community-ports/android9/planet-cosmocom/planet-cosmocom/-/issues"
    icon: "github"
  - name: "Device source"
    link: "https://gitlab.com/ubports/community-ports/android9/planet-cosmocom"
    icon: "gitlab"
  - name: "Kernel source"
    link: "https://github.com/gemian/cosmo-linux-kernel-4.4/tree/ubports"
    icon: "github"
  - name: "Installation instructions"
    link: "https://github.com/gemian/gemian/wiki/UBPorts"
    icon: "github"
---
