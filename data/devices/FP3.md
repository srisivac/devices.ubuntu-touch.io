---
name: "Fairphone 3"
deviceType: "phone"
portType: "Halium 10.0"
buyLink: "https://shop.fairphone.com"
installLink: "https://forums.ubports.com/topic/5939/fairphone-3-3-fp3-fp3-port-halium-10"
enableMdRendering: true
portStatus:
- categoryName: "Actors"
  features:
    - id: "manualBrightness"
      value: "+"
    - id: "notificationLed"
      value: "+"
    - id: "torchlight"
      value: "+"
    - id: "vibration"
      value: "+-"
- categoryName: "Camera"
  features:
    - id: "flashlight"
      value: "+"
    - id: "photo"
      value: "+"
    - id: "video"
      value: "+"
    - id: "switchCamera"
      value: "+"
- categoryName: "Cellular"
  features:
    - id: "carrierInfo"
      value: "+"
    - id: "dataConnection"
      value: "+"
    - id: "dualSim"
      value: "+-"
    - id: "calls"
      value: "+"
    - id: "mms"
      value: "+"
    - id: "pinUnlock"
      value: "+"
    - id: "sms"
      value: "+"
    - id: "audioRoutings"
      value: "+"
    - id: "voiceCall"
      value: "+"
    - id: "volumeControl"
      value: "?"
- categoryName: "Endurance"
  features:
    - id: "batteryLifetimeTest"
      value: "+"
    - id: "noRebootTest"
      value: "?"
- categoryName: "GPU"
  features:
    - id: "uiBoot"
      value: "+"
    - id: "videoAcceleration"
      value: "+"
- categoryName: "Misc"
  features:
    - id: "anboxPatches"
      value: "+"
    - id: "apparmorPatches"
      value: "+"
    - id: "batteryPercentage"
      value: "+"
    - id: "offlineCharging"
      value: "+-"
    - id: "onlineCharging"
      value: "+"
    - id: "recoveryImage"
      value: "+"
    - id: "factoryReset"
      value: "?"
    - id: "sdCard"
      value: "+"
    - id: "rtcTime"
      value: "+"
    - id: "shutdown"
      value: "+"
    - id: "wirelessCharging"
      value: "x"
    - id: "wirelessExternalMonitor"
      value: "-"
- categoryName: "Network"
  features:
    - id: "bluetooth"
      value: "+"
    - id: "flightMode"
      value: "+"
    - id: "hotspot"
      value: "+"
    - id: "nfc"
      value: "?"
    - id: "wifi"
      value: "+"
- categoryName: "Sensors"
  features:
    - id: "autoBrightness"
      value: "+"
    - id: "fingerprint"
      value: "-"
    - id: "gps"
      value: "+"
    - id: "proximity"
      value: "+"
    - id: "rotation"
      value: "+"
    - id: "touchscreen"
      value: "+"
- categoryName: "Sound"
  features:
    - id: "earphones"
      value: "+"
    - id: "loudspeaker"
      value: "+"
    - id: "microphone"
      value: "+"
    - id: "volumeControl"
      value: "+"
- categoryName: "USB"
  features:
    - id: "mtp"
      value: "-"
    - id: "adb"
      value: "-"
    - id: "wiredExternalMonitor"
      value: "x"
deviceInfo:
  - id: 'cpu'
    value: 'Octa-Core Kryo 250 1.8GHz'
  - id: 'chipset'
    value: 'Qualcomm MSM8953 Snapdragon 632'
  - id: 'gpu'
    value: 'Adreno 506 650MHz'
  - id: 'rom'
    value: '64 GB'
  - id: 'ram'
    value: '4 GB'
  - id: 'android'
    value: '9.0 (Pie)'
  - id: 'battery'
    value: '3040 mAh'
  - id: 'display'
    value: '143 mm (5.65 in) : 2160x1080 (427 PPI) - LCD IPS Touchscreen'
  - id: 'rearCamera'
    value: '12 MP, LED flash'
  - id: 'frontCamera'
    value: '8 MP, No flash'
  - id: 'arch'
    value: 'arm64'
  - id: 'dimensions'
    value: '158 mm (6.22 in) (h), 71.8 mm (2.83 in) (w), 9.89 mm (0.39 in) (d)'
  - id: 'weight'
    value: '187,4 g'

contributors:
  - name: 'Luksus'
    photo: 'https://live.staticflickr.com/65535/50801596512_9b846d1a60_k.jpg'
    forum: 'https://forums.ubports.com/user/luksus'

externalLinks:
  - name: "Device Subforum"
    link: "https://forums.ubports.com/category/65/fairphone-3"
    icon: "yumi"  
  - name: 'Repository'
    link: 'https://gitlab.com/ubports/community-ports/android10/fairphone/fairphone_fp3'
    icon: 'github'
  - name: 'CI builds'
    link: 'https://gitlab.com/ubports/community-ports/android10/fairphone/fairphone_fp3/-/pipelines'
    icon: 'github'
---

### Notes

The device is prepared to run the new Anbox replacement called [Waydroid](https://waydro.id/)