---
name: 'Xiaomi Mi A3'
comment: 'wip'
deviceType: 'phone'
image: 'https://fdn2.gsmarena.com/vv/bigpic/xiaomi-mi-a3.jpg'
maturity: .2
---

### Device specifications
|    Component | Details                                                               |
|-------------:|-----------------------------------------------------------------------|
|      Chipset | Qualcomm SDM665 Snapdragon 665                                       |
|          CPU | Octa Core 2.2 GHz Kryo 260                                          |
| Architecture | arm64                                                                 |
|          GPU | Adreno 610                                                            |
|      Display | 720x1560                                                              |
|      Storage | 64 GB / 128 GB                                                   |
| Shipped Android Version | 9.0 |
|       Memory | 4 GB / 6 GB                                                     |
|      Cameras | 48 MP + 8 MP + 2 MP, LED flash<br>32 MP, No flash |
|      Battery | 4030mAh |
|   Dimensions | 153.5 x 71.9 x 8.5 mm |
|       Weight | 173.8 g |
|      MicroSD | Up to 2 TB |
| Release Date | July 2019 |

### Port status
|         Component | Status | Details            |
|------------------:|:------:|--------------------|
|          AppArmor |    Y   |                    |
|      Boot into UI |    Y   |                    |
|            Camera |    Y   | Requires gst-droid |
|    Cellular Calls |    Y   |                    |
|     Cellular Data |    N   |                    |
|               GPS |    N   |                    |
|           Sensors |    Y   |                    |
|             Sound |    Y   |                    |
| UBPorts Installer |    N   |                    |
|  UBPorts Recovery |    N   |                    |
|          Vibrator |    Y   |                    |
|             Wi-Fi |    Y   |                    |

### Maintainer(s)
- mintphin
- maffeen

### Source repos
[https://github.com/mintphin/lunecrash](https://github.com/mintphin/lunecrash)

### Downloads
[https://github.com/mintphin/lunecrash/releases](https://github.com/mintphin/lunecrash)

