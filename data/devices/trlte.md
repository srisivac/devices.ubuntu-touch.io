---
name: "Samsung Galaxy Note 4 (910F, 910P, 910T)"
comment: "community device"
deviceType: "phone"
maturity: .6
deviceInfo:
  - id: "cpu"
    value: "Quad-core 2.7 GHz Krait 450"
  - id: "chipset"
    value: "Qualcomm APQ8084 Snapdragon 805"
  - id: "gpu"
    value: "Qualcomm Adreno 420"
  - id: "rom"
    value: "32GB"
  - id: "ram"
    value: "3GB"
  - id: "android"
    value: "Android 4.4.4"
  - id: "battery"
    value: "3220 mAh"
  - id: "display"
    value: "1440x2560 pixels, 5.7 in"
  - id: "rearCamera"
    value: "16MP"
  - id: "frontCamera"
    value: "3.7MP"
contributors:
  - name: "tigerpro"
    photo: ""
    forum: "https://forums.ubports.com/user/tigerpro"
externalLinks:
  - name: "Device Subforum"
    link: "https://forums.ubports.com/category/61/samsung-galaxy-note-4-910f-p-t"
    icon: "yumi"
  - name: "Halium manifest"
    link: "https://github.com/Halium/halium-devices/blob/halium-7.1/manifests/samsung_trlte.xml"
    icon: "github"
  - name: "trlte device source"
    link: "https://github.com/tyg3rpro/android_device_samsung_trlte"
    icon: "github"
  - name: "trltespr device source"
    link: "https://github.com/tyg3rpro/android_device_samsung_trltespr"
    icon: "github"
  - name: "trltetmo device source"
    link: "https://github.com/tyg3rpro/android_device_samsung_trltetmo"
    icon: "github"
  - name: "trlte common device source"
    link: "https://github.com/tyg3rpro/android_device_samsung_trlte-common"
    icon: "github"
  - name: "Kernel source"
    link: "https://github.com/tyg3rpro/android_kernel_samsung_trlte"
    icon: "github"
---

The Samsung Galaxy Note 4 in variants 910F, 910P, 910T; codename `trlte, trltespr, trltetmo`.

### Preparatory steps

You can install Ubuntu Touch on the Samsung Galaxy Note 4 910F.

1. Make sure your device is [Unlocked](https://forum.xda-developers.com/t/howto-bootloader-unlock-and-upgrade-to-marshmallow-n910vvru2cql1.3398144/) if it is a Verizon Variant.
2. Ensure you have [Installed TWRP](https://twrp.me/samsung/samsunggalaxynote4qualcomm.html).
3. [Enable developer options](https://www.howtogeek.com/129728/how-to-access-the-developer-options-menu-and-enable-usb-debugging-on-android-4.2/) in the Android settings.
4. In the developer options, [enable ADB debugging](https://wiki.lineageos.org/adb_fastboot_guide.html) and OEM unlocking.
