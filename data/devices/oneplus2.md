---
name: 'Oneplus 2'
comment: 'wip'
deviceType: 'phone'
image: 'https://wiki.lineageos.org/images/devices/oneplus2.png'
portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "+"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "?"
      - id: "photo"
        value: "?"
      - id: "video"
        value: "?"
      - id: "switchCamera"
        value: "?"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "+"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "+"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+-"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "+"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "+"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "rtcTime"
        value: "+"
      - id: "sdCard"
        value: "x"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "x"
      - id: "wirelessExternalMonitor"
        value: "?"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+-"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "+-"
      - id: "nfc"
        value: "x"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "+"
      - id: "fingerprint"
        value: "-"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "+"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "+-"
      - id: "adb"
        value: "+-"
      - id: "wiredExternalMonitor"
        value: "x"


externalLinks:
  -
    name: 'Forum Post'
    link: 'https://forums.ubports.com/topic/4253/oneplus-2'
    icon: 'yumi'
---

### Device specifications

|    Component | Details                                                               |
|-------------:|-----------------------------------------------------------------------|
|      Chipset | Qualcomm MSM8994 Snapdragon 810                                       |
|          CPU | Quad-core 1.8 GHz Cortex A57 + quad-core 1.5 GHz Cortex A53           |
| Architecture | arm64                                                                 |
|          GPU | Adreno 430                                                            |
|      Display | 5.5 in 1920 x 1080                                                    |
|      Storage | 16/64 GB                                                              |
|       Memory | 3/4 GB                                                                |
| Rear Camera  | 13 MP, dual-LED flash                                                 |
| Front Camera | 5 MP                                                                  |
| Release Date | July 2015                                                             |
|   Dimensions | 151.8 x 74.9 x 9.85 mm                                                |

### Port status
|         Component | Status | Details            |
|------------------:|:------:|--------------------|
|          AppArmor |    Y   |                    |
|      Boot into UI |    Y   |                    |
|        Bluetooth  |    N   |                    |
|            Camera |    ?   |      Untested      |
|    Cellular Calls |    Y   |                    |
|     Cellular Data |    Y   |                    |
|       Fingerprint |    N   |                    |
|               GPS |    Y   |                    |
|           Sensors |    ?   |      Untested      |
|             Sound |    Y   |                    |
| UBPorts Installer |    N   |      WIP           |
|  UBPorts Recovery |    Y   |                    |
|          Vibrator |    Y   |                    |
|             Wi-Fi |    Y   |                    |


### Maintainer(s)
Vince1171

### Source repos
[Device Tree](https://github.com/Halium/android_device_oneplus_oneplus2)
[Kernel](https://github.com/Halium/android_kernel_oneplus_msm8994)

