---
name: 'Bq Aquaris E4.5'
deviceType: 'phone'
description: 'A solid entry-level smartphone. The Bq E4.5 was one of the first commercially available Ubuntu Touch devices!'
enableMdRendering: true
maturity: .9
---


Note: Bq E4.5 devices that are sold with Android have a locked bootloader, so those need to be [manually unlocked by installing the manufacturers Ubuntu image](https://docs.ubports.com/en/latest/userguide/install.html#install-on-legacy-android-devices) before switching to UBports' release of Ubuntu Touch.
