---
name: 'Bq Aquaris E5'
comment: ''
deviceType: 'phone'
enableMdRendering: true
maturity: .9
---

Bq E5 devices that are sold with Android have a locked bootloader, so those need to be [manually unlocked by installing the manufacturers Ubuntu image](https://docs.ubports.com/en/latest/userguide/install.html#install-on-legacy-android-devices) before switching to UBports' release of Ubuntu Touch.
